#include <Arduino.h>
#include <unity.h>

using namespace fakeit;

void setup(void) {
    ArduinoFakeReset();
}

void test_root(void) {
}

int main(int argc, char** argv) {
    UNITY_BEGIN();

    RUN_TEST(test_root);

    return UNITY_END();
}
