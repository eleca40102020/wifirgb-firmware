/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, May 2018
 **************************************************************************/
#ifndef _WIFIRGB__WSAPI_H
#define _WIFIRGB__WSAPI_H

#include "main.h"

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length);
void handleWSAPI(uint8_t clientNum, JsonObject& requestJson);

#endif
