/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#ifndef _WIFIRGB__RGB_H
#define _WIFIRGB__RGB_H

/**
 * \param red float [0,1]
 * \param green float [0,1]
 * \param blue float [0,1]
 */
struct RGB {
  float red, green, blue;
};

#endif
