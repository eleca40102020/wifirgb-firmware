/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, January 2018
 **************************************************************************/
#ifndef _WIFIRGB__MAIN_H
#define _WIFIRGB__MAIN_H
#define USEOTA

#ifdef UNIT_TEST
    #include <ArduinoFake.h>
#else
    #include <ArduinoJson.h>
#endif

#include "WebSockets.h"

#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#ifdef USEOTA
  #include <ArduinoOTA.h>
#endif

#include <WebSocketsServer.h>
#include "FS.h"
#include <ArduinoJson.h>

#include <WiFiUdp.h>

#include "LEDMode.h"
#include "LEDAnimation.h"
#include "LEDController.h"
#include "HTTPAPI.h"
#include "WSAPI.h"

#define SYSNAME "Vonlatvala WifiRGB"
#define HTTPPORT 80
#define WEBSOCKETPORT 81
#define USE_SERIAL Serial
#define MAX_CONFIG_FILESIZE 2048
#define MAX_JSON_PAYLOAD_SIZE 1024
#define UDPAPIPORT 8000
#define UDP_PACKET_BUFFER_SIZE 255

#define WSAPI_CMD_KEY "cmd"

#define SKIP_SPIFFS false
#define DEFAULT_WIFISSID ""
#define DEFAULT_WIFIPWD ""
#define DEFAULT_HOSTNAME "wifirgb_1"

void setup();
void loop();

extern ESP8266WebServer server;
extern WebSocketsServer webSocket;

extern LEDController ledController;

#endif
