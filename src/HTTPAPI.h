/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, May 2018
 **************************************************************************/
#ifndef _WIFIRGB__HTTPAPI_H
#define _WIFIRGB__HTTPAPI_H

#include "main.h"

void handleRoot();
void handleNotFound();
void handleGetState();
void handleApiSetColor();
void handleApiAnimationBreathe();
void handleRSSI();
void handleApiSysGetLocalIp();

#endif
