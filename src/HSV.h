/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#ifndef _WIFIRGB__HSV_H
#define _WIFIRGB__HSV_H

struct HSV {
  float hue, saturation, value;
};

#endif
