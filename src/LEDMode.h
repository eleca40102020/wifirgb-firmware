/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#ifndef _WIFIRGB__LEDMODE_H
#define _WIFIRGB__LEDMODE_H

enum LEDMode {
  fixed,
  animated
};

#endif
