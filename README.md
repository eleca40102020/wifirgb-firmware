# README #

[![Build Status](https://jenkins.alatvala.fi/buildStatus/icon?job=wifirgb%2Fwifirgb-firmware%2Fmaster)](https://jenkins.alatvala.fi/job/wifirgb/job/wifirgb-firmware/job/master/)

### What is this repository for? ###

* Firmware for ESP8266 WiFi Module
* v0.1
* Includes mDNS, HTTP and WebSockets
* Controls / animates RGB leds

### How do I get set up? ###

* Ensure Python, pip, setuptools and wheel are installed (eg. `apt-get install -y --no-install-recommends python-pip python-setuptools python-wheel && apt-get clean && pip install -U setuptools wheel`)
* Install PlatformIO Core (eg. `pip install -U platformio`)
* Run `pio run -e make` (from repo root dir)

This will build the firmware. There are different environments predefined to use in `platformio.ini`, eg. `d1_mini_pro` (required `--upload-port [path]`) and `ota` (with a hardcoded hostname `wifirgb_dev`, this can be overridden with the parameter `--upload-port` eg. `pio run -e ota --upload-port [hostname].local`)

### How do I run it? ###

* (OSX and Windows): Install [Silicon Labs USB to UART bridge VPC drivers](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)
* Configure the settings in config.json which you will find in ./data
* Run `pio run --target uploadfs`
* To actually upload the firmware thru USB, run `pio run -e d1_mini_pro`


### Hardware ###

https://bitbucket.org/VonLatvala/wifirgbledhardware